RSA Encrypt

Chae Jubb
26 November 2013

To use this software:

###Compiling and Installing###
The files can be compiled simply by typing:
	$make
We can then do a pseudo-install to /usr/local by the command (sudo may be required):
	$make uninstall
The install directory can be overwritten by:
	$make uninstall INSTALL_PATH=<custom install path>

When uninstalling, this option must be passed again.

###Generating Keys###
First generate a set of public and private keys.  NEVER SHARE THE PRIVATE KEY.
You will give the public key to anyone who wishes to send you an encrypted message.
To generate a set of keys:
	$./generateKeys <output prefix>
For example, "./generateKeys chae" will produce chae.public and chae.private, the public and private key files, respectively.


###Encrypting a Message###
In order to encrypt a message, you must have the recipient's public key file.
To encode a message:
	$./encrypt <public key of recipient> <output filename>
You then type your message, terminating it with an EOF (usually ctrl+D). Once you send the EOF, the secret will be stored in the output filename


###Decrypting a Message###
In order to decode a message, you must have the private key that corresponds to the public key used to encrypt the message.
To decode a message:
	$./decrypt <encoded message filename> <private key of recipient>
The message will then be output to standard out.
