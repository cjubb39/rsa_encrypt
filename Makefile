
default: classes encryptc decryptc

# C files
CC=gcc
encryptc: encryptc.c

decryptc: decryptc.c

# Java files
JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
rsa/Makekeys.java \
rsa/Utilities.java \
rsa/Encode.java \
rsa/GenerateMessage.java \
\
io/IO.java

classes: $(CLASSES:.java=.class)


# Install Variables
INSTALL_PATH?=/usr/local

install:
	install -d $(INSTALL_PATH)/bin/
	ln -s `pwd`/generateKeys $(INSTALL_PATH)/bin/
	ln -s `pwd`/encrypt $(INSTALL_PATH)/bin/
	ln -s `pwd`/decrypt $(INSTALL_PATH)/bin/

#Utility Targets
clean:
	$(RM) $(CLASSES:.java=.class) *.o encryptc decryptc

uninstall:
	rm $(INSTALL_PATH)/bin/generateKeys
	rm $(INSTALL_PATH)/bin/encrypt
	rm $(INSTALL_PATH)/bin/decrypt

purge: uninstall clean