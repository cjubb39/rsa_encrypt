package rsa;

import java.io.FileWriter;
import java.io.IOException;

public class GenerateMessage {

	public static final int maxNumber = 4095;
	
	public static void main (String[] args) throws Exception{
		// get input arguments
		FileWriter out = new FileWriter(args[0]);
		int numLines = Integer.parseInt(args[1]);
		
		while(numLines-- > 0){
			out.write(String.valueOf((int) (maxNumber*Math.random())) + "\n");
		}
	}
}
