package rsa;

import io.IO;

public class Encode{
	
	public static void main(String[] args){
		
		// read key and such in
		String[] keyValues = null;
		int key = -1, modulus = -1;
		try{
			keyValues = IO.readFile(args[0]).split("\n");  
			modulus = Integer.parseInt(keyValues[0]);
			key = Integer.parseInt(keyValues[1]);
			if (modulus < key) throw new Exception();
		} catch (Exception e){
			e.printStackTrace();
			System.err.println("IMPROPERLY FORMATTED KEY FILE");
			System.exit(1);
		}
		
		String inString = null;
		while (((inString = IO.prompt(""))) != null && !inString.equals("")){
			System.out.println(Utilities.modularExp(Integer.parseInt(inString), key, modulus));
		}
	}
}