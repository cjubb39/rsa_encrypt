package rsa;
import java.io.FileWriter;
import java.io.IOException;

import io.IO;


public class Makekeys{ 

	public static final int primeLower = 500;
	public static final int primeHigher = 46000;

	public static void main(String[] args){
		if (args.length < 1){
			System.err.printf("Run with one argument: name of keys to be created\n");
		}
		
		String privateKeyName = args[0] + ".private", publicKeyName = args[0] + ".public";
		FileWriter priKey = null, pubKey = null;
		
		// open FileWriters.  Quit if error doing so.
		try{
			priKey = new FileWriter(privateKeyName);
			pubKey = new FileWriter(publicKeyName);
		} catch (IOException e){
			e.printStackTrace();
			System.err.println("ERROR OPENING FILES FOR KEYS");
			System.exit(1);
		}
		
		generateKeys(priKey, pubKey);
	}
	
	/*public static void main(String[] args){
		int test = Integer.parseInt(args[0]);
		System.out.println(Utilities.isPrime(test));
		//System.out.println(modularExp(428689310,666666666,424242));
		int p = generatePrime(64,128), q = generatePrime(64,128);
		System.out.printf("p: %d, q: %d\n", p, q);
	}*/
	
	private static void generateKeys(FileWriter priKey, FileWriter pubKey){
		int p = generatePrime(primeLower, primeHigher);
		int q = generatePrime(primeLower,primeHigher);
		int n = p*q;
		
		int groupSize; int e;
		e = calculateE(groupSize = (p-1)*(q-1));
		
		int d = Utilities.modularInvert(e, groupSize);
//System.out.printf("e: %d; d: %d; gS: %d; ed%%m: %d\n", e, d, groupSize, (e*d) % groupSize);
		
		// write publicKey
		try {
			pubKey.write(String.valueOf(n) + "\n" + String.valueOf(e));
			pubKey.flush();
			priKey.write(String.valueOf(n) + "\n" + String.valueOf(d));
			priKey.flush();
		} catch (IOException excep) {
			excep.printStackTrace();
			System.err.println("ERROR WRITING FILES FOR KEYS");
		} 
	}
	
	private static int generatePrime(int lower, int upper){
		 // randomly check numbers between 64 and 128
		int range = upper - lower, toRet;
		while(!Utilities.isPrime(toRet = (int) (range*Math.random()) + lower));
		return toRet;
	}
	
	private static int calculateE(long number){
		int i = 3; //minimum safe e
		while(Utilities.gcd(number, i++) != 1);
		return i-1;
	}
}