#include <stdio.h>

int main(){
	int rawResponse = 0;
	unsigned char temp = '\0';
	while((rawResponse = fgetc(stdin)) >= 0){
		temp = (unsigned char) rawResponse;
		printf("%03u\n", temp);
	}
}